[![Screenshot](screenshot.png)](https://joshavanier.github.io)

The **Athenaeum** is a personal wiki.

    dat://8fb7911c1e125ad8b771a573da9e267b5f8e19b635a2863b61784bf4e4963e40/

---

**[⬡](https://joshavanier.github.io)**
